import mh_z19

class MH_Z19Sensor:
    def read(self):
        try:
            out = mh_z19.read()
            return out
        except OSError:
            return 0