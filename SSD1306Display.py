import time

import board
import adafruit_ssd1306
import digitalio
 
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
 
import subprocess

class SSD1306Display:
    def __init__(self):
        # Raspberry Pi pin configuration:
        self.oled_reset = digitalio.DigitalInOut(board.D4)

        # Change these
        # to the right size for your display!
        self.WIDTH = 128
        #HEIGHT = 32  # Change to 64 if needed
        self.HEIGHT = 64  # Change to 64 if needed
        self.BORDER = 5
        
        self.i2c = board.I2C()
        # Note you can change the I2C address by passing an i2c_address parameter like:
        self.disp = adafruit_ssd1306.SSD1306_I2C(self.WIDTH, self.HEIGHT, self.i2c, addr=0x3C, reset=self.oled_reset)

        # Clear display
        self.disp.fill(0)
        self.disp.show()
        
        # Create blank image for drawing.
        # Make sure to create image with mode '1' for 1-bit color.
        self.width = self.disp.width
        self.height = self.disp.height
        self.image = Image.new('1', (self.width, self.height))
        
        # Get drawing object to draw on image.
        self.draw = ImageDraw.Draw(self.image)
        
        # Draw a black filled box to clear the image.
        self.draw.rectangle((0,0,self.width,self.height), outline=0, fill=0)
        
        # Draw some shapes.
        # First define some constants to allow easy resizing of shapes.
        self.padding = 1
        self.top = self.padding
        self.bottom = self.height-self.padding
        # Move left to right keeping track of the current x position for drawing shapes.
        self.x = 0
        
        # Load default font.
        self.font = ImageFont.load_default()
        self.font = ImageFont.truetype("fonts-japanese-gothic.ttf",11)

    def dispTexts(self, texts):
        # Draw a black filled box to clear the image.
        self.draw.rectangle((0,0,self.width,self.height), outline=0, fill=0)
    
        # Write two lines of text.
        for index in range(len(texts)):
            if 8 * index > self.HEIGHT:
                break
            self.draw.text((self.x, self.top + 12 * index), texts[index],  font=self.font, fill=255)
    
        # Display image.
        self.disp.image(self.image)
        self.disp.show()