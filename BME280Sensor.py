import smbus2
import bme280

class BME280Sensor:
    def __init__(self):
        self.port = 1
        self.address = 0x76
        self.bus = smbus2.SMBus(self.port)

    def sample(self):
        calibration_params = bme280.load_calibration_params(self.bus, self.address)

        # the sample method will take a single reading and return a
        # compensated_reading object
        try:
            data = bme280.sample(self.bus, self.address, calibration_params)
            return data
        except OSError:
            return 0