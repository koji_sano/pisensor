from BME280Sensor import BME280Sensor
from MH_Z19Sensor import MH_Z19Sensor
from SSD1306Display import SSD1306Display
import time
from elasticsearch import Elasticsearch
from datetime import datetime, timedelta, timezone
import uuid
from elasticsearch.exceptions import ConnectionError
import asyncio
import argparse
import configparser

class Pisensor:
   
    def myInit(self):
        failed = True
        while failed:
            try:
                self.bme = BME280Sensor()
                self.mhz = MH_Z19Sensor()
                self.disp = SSD1306Display()
                failed = False
                print("Init success.")
            except (OSError, ValueError) as e:
                print("Init error:",e)
                time.sleep(10)


    def updateDisp(self, date, temp, humid, pres, co2):

        self.disp.dispTexts(["{0}".format(date),\
            "Temp:  {0:.2f} °C".format(temp),\
            "Humid: {0:.2f} %".format(humid),\
            "Pres:  {0:.2f} hPa".format(pres),\
            "CO2:   {0} ppm".format(co2)
            ])
    
    def getData(self):
        bmeData = self.bme.sample()
        mhzData = self.mhz.read()
        
        date = ""
        temp = 0
        humid = 0
        pres = 0
        co2 = None

        if bmeData != 0:
            date = bmeData.timestamp.strftime('%Y/%m/%d %H:%M:%S')
            temp = bmeData.temperature
            humid = bmeData.humidity
            pres = bmeData.pressure

        if mhzData != 0 and mhzData != None:
            co2 = mhzData['co2']

        return date, temp, humid, pres, co2

    def recovery(self):
        failed = True
        while failed:
            try:
                self.myInit()
                failed = False
                print("Recovery success.")
            except (OSError, ValueError) as e:
                print("Recovery error:",e)
                time.sleep(10)

    async def sendData2ElasticSearch(self, esurl, temp, humid, pres, co2):
        JST = timezone(timedelta(hours=+9), 'JST')

        es = Elasticsearch(esurl)

        mapping = {
            "mappings": {
                "properties": {
                    "timestamp": {"type": "date"},
                    "temperature": {"type": "double"},
                    "humidity": {"type": "double"},
                    "pressure": {"type": "double"},
                    "co2": {"type": "long"}
                }
            }
        }

        today = datetime.now().strftime("%Y%m%d")
        aqIndexName = "airquality-"+today

        if not es.indices.exists(index=aqIndexName):
            es.indices.create(index=aqIndexName, body=mapping)

        aqData = {
            "timestamp": datetime.now(tz=JST),
            "temperature": temp,
            "humidity": humid,
            "pressure": pres,
            "co2": co2
        }

        res = es.create(index=aqIndexName, id=uuid.uuid4(),body=aqData)
        print(res)
        es.close()

async def main():
    confparser = configparser.ConfigParser()
    argparser = argparse.ArgumentParser(description="RaspberyPi Sensor(pisensor).")
    argparser.add_argument('--conf', dest='conf', required=True, help='Path to config file(e.g. /etc/pisensor.conf)')

    args = argparser.parse_args()
    confparser.read(args.conf)
    esurl = confparser.get('Elasticsearch','url')

    sensor = Pisensor()
    sensor.myInit()
    lastESpost = 0

    while True:
        try:
            date, temp, humid, pres, co2 = sensor.getData()
            sensor.updateDisp(date, temp, humid, pres, co2)
            now = time.time()
            if now - lastESpost > 5 * 60:
                try:
                    await asyncio.wait_for(sensor.sendData2ElasticSearch(esurl, temp, humid, pres, co2), timeout=10)
                except asyncio.TimeoutError:
                    print('Timeout for send index to Elasticsearch.')
                lastESpost = now
        except (OSError, ValueError, ConnectionError) as e:
            print('Catch error:',e)
            sensor.recovery()
        
        time.sleep(1)


asyncio.run(main())