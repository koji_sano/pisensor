from elasticsearch import Elasticsearch
from datetime import datetime, timedelta, timezone
import uuid

JST = timezone(timedelta(hours=+9), 'JST')

es = Elasticsearch("http://rpi-elasticsearch.local:9200")

mapping = {
    "mappings": {
        "properties": {
            "timestamp": {"type": "date"},
            "temperature": {"type": "double"},
            "humidity": {"type": "double"},
            "pressure": {"type": "double"},
            "co2": {"type": "long"}
        }
    }
}

today = datetime.now().strftime("%Y%m%d")
aqIndexName = "airquality-"+today

if not es.indices.exists(index=aqIndexName):
    es.indices.create(index=aqIndexName, body=mapping)

aqData = {
    "timestamp": datetime.now(tz=JST),
    "temperature": 20.1,
    "humidity": 49.05,
    "pressure": 1014.01,
    "co2": 756
}

res = es.create(index=aqIndexName, id=uuid.uuid4(),body=aqData)
query = {
    "query": {
        "match_all": {}
    }
}
res = es.search(index="airquality-*", body=query)

print(res)

es.close()