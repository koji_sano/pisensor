# setup

sudo pip3 install RPi.bme280
sudo pip3 install mh-z19
sudo pip3 install adafruit-circuitpython-ssd1306
sudo pip3 install elasticsearch

# how to run script

sudo python3 pisensor.py

# sensors how to

## how to use bme280

https://pypi.org/project/RPi.bme280/


## how to use mh-z19

https://pypi.org/project/mh-z19/
https://dev.classmethod.jp/articles/raspberry-pi-4-b-mh-z19b-co2/


## how to use ssd1306 oled display

https://learn.adafruit.com/monochrome-oled-breakouts/python-setup

## how to use Elasticsearch

https://qiita.com/satto_sann/items/8a63761bbfd6542bb9a2